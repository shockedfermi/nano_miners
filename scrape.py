#!/usr/bin/env python3

from selenium import webdriver
import pandas as pd
import numpy as np
import pickle
import datetime
import os

d1 = datetime.date.today().strftime("%d_%m_%Y")
print( d1 )

os.makedirs( d1, exist_ok = True )

url = "https://eth.2miners.com/miners"

n_bins = 60

get_old = False

if get_old:
    df = pickle.load( open("last_dataframe.pickle", "rb") )
else:
    firefox_profile = webdriver.FirefoxProfile()
    firefox_profile.set_preference('permissions.default.image', 2)
    firefox_profile.set_preference('dom.ipc.plugins.enabled.libflashplayer.so', 'false')
    options = webdriver.firefox.options.Options()
    options.headless = False
    driver  = webdriver.Firefox( firefox_profile, options = options )

    print("user agent: ", driver.execute_script("return navigator.userAgent") )
    
    print("Doing url: " + url )
    
    driver.get( url )

    table = driver.find_element_by_xpath("//table[@class='table miner-table col3-muted']")
    df = pd.read_html( table.get_attribute('outerHTML'))[0]

    driver.close()

import matplotlib.pyplot as pl

nanos = []
btcs = []
eths = []

eth_length =     len("0xcf57b1311904d00f4fc9ca625dba07d4bf8d7837")
nano_lengths = [ len("nano_39ie5xjpuhzuys8k8ig8pqqigjd4frmz4cm7n6p69dxqgqpar1fij6sa7ogq"),
                 len("xrb_18o48i3eugt7f1cbi3wyztn7uhjruotir74k1sndxp7dszqiami5m91gdo3d")]
btc_lengths = [  len("1DKcBdvCZ11Mx5hGjqkvMb1CkMM9BgZbtd"),
                 len("1rd6i9qPkCuSyrU3itL5Ve4U7TUiEdWBa"),
                 len("bc1qntd3xs6z7qj6u8fdppnmzv7n6qpp46gqdw0jeastyltt6q5akdpsvplsya"),
                 len("bc1qyy08wuvq9dzjtcjjfhe6xdc20w2repmz8mun93") ]

for index, row in df.iterrows():
    
    hash_rate_field = row.Hashrate
    hash_rate_val = float(hash_rate_field.split(" ")[0])
    hash_rate_unit = hash_rate_field.split(" ")[1]
    if   hash_rate_unit == "TH/s": hash_rate_val *= 1e6
    elif hash_rate_unit == "GH/s": hash_rate_val *= 1e3
    elif hash_rate_unit == "MH/s": hash_rate_val *= 1.0
    elif hash_rate_unit == "KH/s": hash_rate_val *= 1e-3

    #if row.Miner[0:2] not in [ "0x", "na", "xr", "1D", "1r", "bc" ]:
    #    print( "new adress:", row.Miner )
    if   row.Miner[0:2] == "0x": eths.append(  hash_rate_val )
    elif len(row.Miner) in nano_lengths : nanos.append( hash_rate_val )
    elif len(row.Miner) in btc_lengths : btcs.append(  hash_rate_val )
    else: print( "unknown address format: {}".format( row.Miner ) )

#pl.hist( eths + nanos + btcs, label = "Total", color = 'black', bins = np.logspace( 1, 7, n_bins ) )
pl.hist( [nanos, btcs, eths], label = ["Nano payout {}".format( len(nanos) ), "Bitcoin payout {}".format( len(btcs) ), "Ethereum payout {}".format( len(eths) )], stacked = True, color = ['blue', 'orange', 'purple'], alpha = 0.3, bins = np.logspace( 1, 4, n_bins ) )
pl.legend()
pl.xlabel( "Hash rate [MH/s]" )
pl.title( "Stacked number of miners " + d1 )
pl.xscale( 'log' )
pl.savefig(d1+"/Stacked_miners.pdf")
pl.savefig(d1+"/Stacked_miners.png")
pl.ylabel( "Number of miners" )
pl.clf()

pl.hist( nanos+btcs+eths, label = "Total {}".format( len(nanos) + len(eths) + len(btcs) ), color = 'black', bins = np.logspace( 1, 7, n_bins ), histtype='step' )
pl.hist( [nanos, btcs, eths], label = ["Nano payout {}".format( len(nanos) ), "Bitcoin payout {}".format( len(btcs) ), "Ethereum payout {}".format( len(eths) )], color = ['blue', 'orange', 'purple'], alpha = 0.3, histtype = "stepfilled", bins = np.logspace( 1, 7, n_bins ) )
pl.legend()
pl.xlabel( "Hash rate [MH/s]" )
pl.title( "Superimposed number of miners " + d1 )
pl.xscale( 'log' )
pl.yscale( 'log' )
pl.ylabel( "Number of miners" )
pl.savefig(d1+"/Total_miners.pdf")
pl.savefig(d1+"/Total_miners.png")
pl.clf()

pl.hist( [nanos, btcs, eths], label = ["Nano payout {}".format( len(nanos) ), "Bitcoin payout {}".format( len(btcs) ), "Ethereum payout {}".format( len(eths) )], color = ['blue', 'orange', 'purple'], alpha = 0.3, histtype = "stepfilled", bins = np.logspace( 1, 4, n_bins ), weights = [np.ones_like(nanos)/len(nanos), np.ones_like(btcs)/len(btcs), np.ones_like(eths)/len(eths)] )
pl.legend()
pl.title( "Miners normalized to payout coin number " + d1 )
pl.xscale( 'log' )
pl.xlabel( "Hash rate [MH/s]" )
pl.ylabel( "Number of miners/Number of miners on coin X" )
pl.savefig(d1+"/Total_miners_norm.pdf")
pl.savefig(d1+"/Total_miners_norm.png")
    
if not get_old: pickle.dump( df, open("last_dataframe.pickle", "wb") )


